#!/usr/bin/python3
import os
import sys
#import yaml
import tarfile
import tempfile

import importlib.util
spec = importlib.util.spec_from_file_location("Package", os.path.join( os.environ['CI_PROJECT_DIR'], "ci-utilities", "components", "Package.py") )
Package = importlib.util.module_from_spec(spec)
sys.modules["Package"] = Package
spec.loader.exec_module(Package)

#from ../ci-utilities/components import CommonUtils, Dependencies, Package, EnvironmentHandler, TestHandler, PlatformFlavor

# Retrieve some key bits of information from our environment
# All of these come from environment variables due to needing to be set on either the CI Agent level or the group project level
# We remove them from the environment as they are sensitive (in the case of KDECI_GITLAB_TOKEN especially) and aren't needed by anything else
# The remainder are required for storing packages locally and fetching them
localCachePath = os.environ.pop('KDECI_CACHE_PATH')
gitlabInstance = os.environ.pop('KDECI_GITLAB_SERVER')
gitlabToken    = os.environ.pop('KDECI_GITLAB_TOKEN', None)
packageProject = os.environ.pop('KDECI_PACKAGE_PROJECT')

####
# Capture the installation if needed and deploy the staged install to the final install directory
####

# Create a temporary file, then open the file as a tar archive for writing
# We don't want it to be deleted as storePackage will move the archive into it's cache
archiveFile = tempfile.NamedTemporaryFile(delete=False)
archive = tarfile.open( fileobj=archiveFile, mode='w' )

# Now determine the path we should be archiving
# Because we could potentially be running on Windows we have to ensure our second path has been converted to a suitable form
# This conversion is necessary as os.path.join can't handle the presence of drive letters in paths other than the first argument
#pathToArchive = os.path.join( installStagingPath, CommonUtils.makePathRelative(installPath) )
pathToArchive = os.path.join( os.environ['CI_PROJECT_DIR'], os.environ['KDECI_CRAFT_PLATFORM'] )

# Add all the files which need to be in the archive into the archive
# We want to capture the tree as it is inside the install directory and don't want any trailing slashes in the archive as this isn't standards compliant
# Therefore we list everything in the install directory and add each of those to the archive, rather than adding the whole install directory
filesToInclude = os.listdir( pathToArchive )
for filename in filesToInclude:
    fullPath = os.path.join(pathToArchive, filename)
    archive.add( fullPath, arcname=filename, recursive=True )

# Close the archive, which will write it out to disk, finishing what we need to do here
archive.close()
archiveFile.close()

# Bring the package archive up
packageRegistry = Package.Registry( localCachePath, gitlabInstance, gitlabToken, packageProject )

print("## PUBLISH TO ARCHIVE")
# Are we supposed to be publishing this particular package to the archive?
if gitlabToken is not None:
    # With the archive being generated, we can now prepare some metadata...
    packageMetadata = {
        'dependencies': {},
        'runtime-dependencies': {}
    }
    
    # Grab the Git revision (SHA-1 hash) we are building
    # This is always present in Gitlab CI builds
    gitRevision = os.environ['CI_COMMIT_SHA']

    print("## publish to project " + os.environ['CI_PROJECT_NAME'])
    # Publish our package to the
    packageRegistry.upload(archiveFile.name, os.environ['CI_PROJECT_NAME'], os.environ['CI_COMMIT_REF_NAME'], gitRevision, packageMetadata)
else:
    if gitlabToken is None:
        print("## skip, because GitLab token is missing.")
